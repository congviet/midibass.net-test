﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Midi;
//using MidiTest.BASSMIDI;

namespace MidiTest
{
    public static class ModMidiTest
    {
//=================================================================================
//modMidiTest.bas - Copyright (c) 2006-2010 (: JOBnik! :) [Arthur Aminov, ISRAEL]
//                                                        [http://www.jobnik.org]
//                                                        [  jobnik@jobnik.org  ]
//Other sources: frmMidiTest.frm
//
//BASSMIDI test
//* Requires: BASS 2.4 (available @ www.un4seen.com)
//=================================================================================

        public static int chan; //channel handle
        public static int font; //soundfont
        public static int miditempo; //MIDI file tempo
        public static float temposcale; //tempo adjustment
        public static string lyrics; //lyrics buffer
        public static frmMidiTest frm;

        [DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
        private static extern void CopyMemory(ref object Destination, ref object Source, long length);

        public static void LyricSync(int handle, int channel, int data, IntPtr user)
        {
            try
            {
                var mark = new BASS_MIDI_MARK();

                BassMidi.BASS_MIDI_StreamGetMark(channel, (BASSMIDIMarker) user, data, mark); //get the lyric/text

                var text = mark.text;
                if (text.Substring(0, 1) == "@") return; //skip info

                int tmp;

                if (text.Substring(0, 1) == "\\") //clear display
                {
                    lyrics = "";
                    text = //Right(text, Len(text) - 1)
                        text.Substring(1);
                }
                else if (text.Substring(0, 1) == "/") //new line
                {
                    lyrics = lyrics + "\r\n";
                    text = //Right(text, Len(text) - 1)
                        text.Substring(1);
                }

                //count lines
                long lines = 0;
                for (tmp = 0; tmp < lyrics.Length;)
                {
                    tmp = //InStr(tmp, lyrics, vbCrLf)
                        lyrics.IndexOf(Environment.NewLine, tmp, StringComparison.Ordinal);
                    if (tmp == -1) break;
                    lines++;
                }

                //remove old lines so that new lines fit in display
                if (lines > 3) lyrics = "";

                lyrics = lyrics + text;
                if (frm.InvokeRequired)
                    frm.Invoke(new MethodInvoker(delegate { frm.lblLyrics.Text = lyrics; }));
                else
                    frm.lblLyrics.Text = lyrics;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace, "Error");
            }
        }

        public static void EndSync(int handle, int channel, int data, IntPtr user)
        {
            lyrics = ""; //clear lyrics
            if (frm.InvokeRequired)
                frm.Invoke(new MethodInvoker(delegate { frm.lblLyrics.Text = ""; }));
            else
                frm.lblLyrics.Text = "";
        }

        public static void SetTempo(bool reset)
        {
            if (reset)
                miditempo = BassMidi.BASS_MIDI_StreamGetEvent(chan, 0, BASSMIDIEvent.MIDI_EVENT_TEMPO);
                    //get the file's tempo
            BassMidi.BASS_MIDI_StreamEvent(chan, 0, BASSMIDIEvent.MIDI_EVENT_TEMPO, (int) (miditempo*temposcale));
                //set tempo
        }

        public static void TempoSync(int handle, int channel, int data, IntPtr user)
        {
            SetTempo(true); //override the tempo
        }

        //look for a marker (eg. loop points)
        public static bool FindMarker(int handle, string text, ref BASS_MIDI_MARK mark)
        {
            for (var a = 0;
                BassMidi.BASS_MIDI_StreamGetMark(handle, BASSMIDIMarker.BASS_MIDI_MARK_MARKER, a, mark);
                a++)
            {
                if (mark.text == text) //found it
                    return true;
            }
            return false;
        }

        public static void LoopSync(int handle, int channel, int data, IntPtr user)
        {
            try
            {
                var mark = new BASS_MIDI_MARK();

                if (FindMarker(channel, "loopstart", ref mark)) //found a loop start point
                    Bass.BASS_ChannelSetPosition(channel, mark.pos,
                        BASSMode.BASS_POS_BYTES | BASSMode.BASS_MIDI_DECAYSEEK);
                        //rewind to it (and let old notes decay)
                else
                    Bass.BASS_ChannelSetPosition(channel, 0,
                        BASSMode.BASS_POS_BYTES | BASSMode.BASS_MIDI_DECAYSEEK); //else rewind to the beginning instead
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace, "Error");
            }
        }
    }
}