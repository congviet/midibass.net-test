﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Un4seen.Bass;
using Un4seen.Bass.AddOn.Midi;

namespace MidiTest
{
    public partial class frmMidiTest : Form
    {
        public frmMidiTest()
        {
            InitializeComponent();
            ModMidiTest.frm = this;
        }

        private void Error_(string es)
        {
            MessageBox.Show(es + @"\r\n(error code: " + Bass.BASS_ErrorGetCode() + @")", @"Error", MessageBoxButtons.OK,
                MessageBoxIcon.Exclamation);
        }

        private void chkFX_Click(object sender, EventArgs e)
        {
            if (chkFX.Checked)
                Bass.BASS_ChannelFlags(ModMidiTest.chan, 0, BASSFlag.BASS_MIDI_NOFX); // enable FX
            else
                Bass.BASS_ChannelFlags(ModMidiTest.chan, BASSFlag.BASS_MIDI_NOFX, BASSFlag.BASS_MIDI_NOFX); // disable FX
        }

        private void cmdOpenFP_Click(object sender, EventArgs e)
        {
            try
            {
                // in case Cancel was pressed

                CMD.CheckFileExists = true;
                CMD.ShowReadOnly = true;
                CMD.Filter = @"MIDI files (mid/midi/rmi/kar)|*.mid;*.midi;*.rmi;*.kar|All files|*.*";
                if (CMD.ShowDialog() != DialogResult.OK) // if cancel was pressed, exit the procedure
                    return;

                Bass.BASS_StreamFree(ModMidiTest.chan); // free old stream before opening new
                lblLyrics.Text = ""; // clear lyrics display

                ModMidiTest.chan = BassMidi.BASS_MIDI_StreamCreateFile(CMD.FileName, 0, 0,
                    BASSFlag.BASS_SAMPLE_LOOP | (chkFX.Checked ? 0 : BASSFlag.BASS_MIDI_NOFX), 1);

                // it ain//t a MIDI
                if (ModMidiTest.chan == 0)
                {
                    cmdOpenFP.Text = @"click here to open a file && play it...";
                    lblTitle.Text = "";
                    Error_("Can't play the file");
                    return;
                }

                // update the button to show the loaded file name
                cmdOpenFP.Text = GetFileName(CMD.FileName);

                // set the title (first text in first track)
                lblTitle.Text = Marshal.PtrToStringAnsi(Bass.BASS_ChannelGetTags(ModMidiTest.chan, BASSTag.BASS_TAG_MIDI_TRACK));

                // update pos scroller range (using tick length)
                sldPosition.Maximum = (int)(Bass.BASS_ChannelGetLength(ModMidiTest.chan, BASSMode.BASS_POS_MIDI_TICK) / 120);
                sldPosition.Value = 0;

                // set looping syncs
                BASS_MIDI_MARK mark = new BASS_MIDI_MARK();
                if (ModMidiTest.FindMarker(ModMidiTest.chan, "loopend", ref mark)) // found a loop end point
                    Bass.BASS_ChannelSetSync(ModMidiTest.chan, BASSSync.BASS_SYNC_POS | BASSSync.BASS_SYNC_MIXTIME, mark.pos,
                        ModMidiTest.LoopSync, IntPtr.Zero); // set a sync there

                Bass.BASS_ChannelSetSync(ModMidiTest.chan, BASSSync.BASS_SYNC_END | BASSSync.BASS_SYNC_MIXTIME, 0, ModMidiTest.LoopSync,
                    IntPtr.Zero); // set one at the end too (eg. in case of seeking past the loop point)

                // clear lyrics buffer and set lyrics syncs
                ModMidiTest.lyrics = "";

                if (BassMidi.BASS_MIDI_StreamGetMark(ModMidiTest.chan, BASSMIDIMarker.BASS_MIDI_MARK_LYRIC, 0, mark)) // got lyrics
                    Bass.BASS_ChannelSetSync(ModMidiTest.chan, BASSSync.BASS_SYNC_MIDI_MARKER,
                        (long)BASSMIDIMarker.BASS_MIDI_MARK_LYRIC, ModMidiTest.LyricSync,
                        (IntPtr) BASSMIDIMarker.BASS_MIDI_MARK_LYRIC);
                else if (BassMidi.BASS_MIDI_StreamGetMark(ModMidiTest.chan, BASSMIDIMarker.BASS_MIDI_MARK_TEXT, 20, mark))
                     // got text instead (over 20 of them)
                    Bass.BASS_ChannelSetSync(ModMidiTest.chan, BASSSync.BASS_SYNC_MIDI_MARKER, (long)BASSMIDIMarker.BASS_MIDI_MARK_TEXT,
                    ModMidiTest.LyricSync, (IntPtr)BASSMIDIMarker.BASS_MIDI_MARK_TEXT);
                Bass.BASS_ChannelSetSync(ModMidiTest.chan, BASSSync.BASS_SYNC_END, 0, ModMidiTest.EndSync, IntPtr.Zero);

                // override the initial tempo, and set a sync to override tempo events and another to override after seeking
                ModMidiTest.SetTempo(true);
                Bass.BASS_ChannelSetSync(ModMidiTest.chan, BASSSync.BASS_SYNC_MIDI_EVENT | BASSSync.BASS_SYNC_MIXTIME,
                    (long)BASSMIDIEvent.MIDI_EVENT_TEMPO, ModMidiTest.TempoSync, IntPtr.Zero);
                Bass.BASS_ChannelSetSync(ModMidiTest.chan, BASSSync.BASS_SYNC_SETPOS | BASSSync.BASS_SYNC_MIXTIME, 0, ModMidiTest.TempoSync,
                    IntPtr.Zero);

                // get default soundfont in case of matching soundfont being used
                int fc = BassMidi.BASS_MIDI_StreamGetFontsCount(0);
                BASS_MIDI_FONT[] sf;
                if (fc > 0)
                    sf = new BASS_MIDI_FONT[fc];
                else sf = null;

                BassMidi.BASS_MIDI_StreamGetFonts(ModMidiTest.chan, sf, 1);
                if (sf != null && sf.Length > 0)
                    ModMidiTest.font = sf[0].font;

                Bass.BASS_ChannelPlay(ModMidiTest.chan, false);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace,"error");
            }
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            try
            {
                // in case Cancel was pressed

                CMD.CheckFileExists = true;
                CMD.ShowReadOnly = true;
                CMD.Filter = @"Soundfonts (sf2/sf2pack)|*.sf2;*.sf2pack|All files|*.*";
                if (CMD.ShowDialog() != DialogResult.OK) // if cancel was pressed, exit the procedure
                    return;


                var newfont = BassMidi.BASS_MIDI_FontInit(CMD.FileName, 0);
                if (newfont != 0)
                {
                    BASS_MIDI_FONT[] sf = new BASS_MIDI_FONT[1];
                    sf[0].font = newfont;
                    sf[0].preset = -1; // use all presets
                    sf[0].bank = 0; // use default bank(s)

                    BassMidi.BASS_MIDI_StreamSetFonts(0, sf, 1); // set default soundfont
                    BassMidi.BASS_MIDI_StreamSetFonts(ModMidiTest.chan, sf, 1); // set for current stream too
                    BassMidi.BASS_MIDI_FontFree((int)ModMidiTest.font); // free old soundfont
                    ModMidiTest.font = newfont;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace,"error");
            }
        }

        private void sldPosition_MouseDown(object sender, MouseEventArgs e)
        {
            tmrMidiTest.Enabled = false;
        }

        private void sldPosition_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                Bass.BASS_ChannelSetPosition(ModMidiTest.chan, sldPosition.Value*120, BASSMode.BASS_POS_MIDI_TICK);
                // set the position
                ModMidiTest.lyrics = ""; // clear lyrics
                lblLyrics.Text = "";
                tmrMidiTest.Enabled = true;
            }
            catch (Exception exx)
            {
                MessageBox.Show(exx.Message + "\r\n" + exx.StackTrace, "error");
            }
        }

        private void sldTempo_Scroll(object sender, EventArgs e)
        {
            ModMidiTest.temposcale = 1 / ((30 - sldTempo.Value) / sldTempo.Maximum); // up to +/- 50% bpm
            ModMidiTest.SetTempo(false); // apply the tempo adjustment
            sldTempo.Text = (sldTempo.Value - sldTempo.Maximum/2)*-1*5 + "%";
        }

        private void tmrMidiTest_Timer(object sender, EventArgs e)
        {
            try
            {
                if (ModMidiTest.chan != 0)
                {
                    sldPosition.Value =
                        (int) (Bass.BASS_ChannelGetPosition(ModMidiTest.chan, BASSMode.BASS_POS_MIDI_TICK)/120);
                    // update position
                    lblBPM.Text = string.Format("{0}.0", 60000000/(ModMidiTest.miditempo*ModMidiTest.temposcale));
                    // calculate bpm
                }

                string text;
                long updatefont = 0;

                updatefont = updatefont + 1;
                if (updatefont == 1) // only updating font info once a second
                {
                    text = "no soundfont";

                    BASS_MIDI_FONTINFO i = new BASS_MIDI_FONTINFO();

                    if (BassMidi.BASS_MIDI_FontGetInfo(ModMidiTest.font, i))
                        text = "name: " + i.name + "\r\n" + "loaded: " + i.samload + " / " + i.samsize;


                    lblSoundfont.Text = text;
                    updatefont = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\r\n" + ex.StackTrace, "Error");
            }
        }

        private string GetFileName(string fp)
        {
            return fp.Substring(fp.IndexOf("\\", StringComparison.Ordinal) + 1);
        }


        private void frmMidiTest_FormClosing(object sender, FormClosingEventArgs e)
        {
            Bass.BASS_Free(); // free BASS
            Bass.BASS_PluginFree(0);
        }


        private void frmMidiTest_Load(object sender, EventArgs e)
        {
            // change and set the current path, to prevent from VB not finding BASS.DLL
            // check the correct BASS was loaded
            if (Utils.HighWord(Bass.BASS_GetVersion()) != Bass.BASSVERSION)
            {
                MessageBox.Show(@"An incorrect version of BASS.DLL was loaded", @"Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                return;
            }
            // setup output - default device, 44100hz, stereo, 16 bits
            if (!Bass.BASS_Init(-1, 44100, 0, this.Handle, Guid.Empty))
            {
                Error_("Can't initialize device");
                return;
            }

            // get default font (28mbgm.sf2/ct8mgm.sf2/ct4mgm.sf2/ct2mgm.sf2 if available)

            BASS_MIDI_FONT[] sf = BassMidi.BASS_MIDI_StreamGetFonts(0);

            if (sf != null && sf.Length > 0) ModMidiTest.font = sf[0].font;

            // load optional plugins for packed soundfonts (others may be used too)
            Bass.BASS_PluginLoad("bassflac.dll");
            Bass.BASS_PluginLoad("basswv.dll");

            // tempo adjustment
            ModMidiTest.temposcale = 1;
        }
    }
}