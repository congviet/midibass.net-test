﻿namespace MidiTest
{
    partial class frmMidiTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.frameTempo = new System.Windows.Forms.GroupBox();
            this.lblBPM = new System.Windows.Forms.Label();
            this.sldTempo = new System.Windows.Forms.TrackBar();
            this.chkFX = new System.Windows.Forms.CheckBox();
            this.tmrMidiTest = new System.Windows.Forms.Timer(this.components);
            this.frameSoundfont = new System.Windows.Forms.GroupBox();
            this.lblSoundfont = new System.Windows.Forms.Label();
            this.btnReplace = new System.Windows.Forms.Button();
            this.frameLyrics = new System.Windows.Forms.GroupBox();
            this.lblLyrics = new System.Windows.Forms.Label();
            this.sldPosition = new System.Windows.Forms.TrackBar();
            this.cmdOpenFP = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.CMD = new System.Windows.Forms.OpenFileDialog();
            this.frameTempo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldTempo)).BeginInit();
            this.frameSoundfont.SuspendLayout();
            this.frameLyrics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldPosition)).BeginInit();
            this.SuspendLayout();
            // 
            // frameTempo
            // 
            this.frameTempo.Controls.Add(this.lblBPM);
            this.frameTempo.Controls.Add(this.sldTempo);
            this.frameTempo.Location = new System.Drawing.Point(368, 58);
            this.frameTempo.Name = "frameTempo";
            this.frameTempo.Size = new System.Drawing.Size(57, 133);
            this.frameTempo.TabIndex = 0;
            this.frameTempo.TabStop = false;
            this.frameTempo.Text = " Tempo ";
            // 
            // lblBPM
            // 
            this.lblBPM.AutoSize = true;
            this.lblBPM.Location = new System.Drawing.Point(6, 16);
            this.lblBPM.Name = "lblBPM";
            this.lblBPM.Size = new System.Drawing.Size(0, 13);
            this.lblBPM.TabIndex = 2;
            // 
            // sldTempo
            // 
            this.sldTempo.Location = new System.Drawing.Point(6, 29);
            this.sldTempo.Maximum = 20;
            this.sldTempo.Name = "sldTempo";
            this.sldTempo.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.sldTempo.Size = new System.Drawing.Size(45, 104);
            this.sldTempo.TabIndex = 1;
            this.sldTempo.TickFrequency = 10;
            this.sldTempo.Value = 10;
            this.sldTempo.Scroll += new System.EventHandler(this.sldTempo_Scroll);
            // 
            // chkFX
            // 
            this.chkFX.AutoSize = true;
            this.chkFX.Checked = true;
            this.chkFX.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFX.Location = new System.Drawing.Point(262, 58);
            this.chkFX.Name = "chkFX";
            this.chkFX.Size = new System.Drawing.Size(106, 17);
            this.chkFX.TabIndex = 1;
            this.chkFX.Text = "Reverb && Chorus";
            this.chkFX.UseVisualStyleBackColor = true;
            this.chkFX.Click += new System.EventHandler(this.chkFX_Click);
            // 
            // tmrMidiTest
            // 
            this.tmrMidiTest.Enabled = true;
            this.tmrMidiTest.Interval = 500;
            this.tmrMidiTest.Tick += new System.EventHandler(this.tmrMidiTest_Timer);
            // 
            // frameSoundfont
            // 
            this.frameSoundfont.Controls.Add(this.lblSoundfont);
            this.frameSoundfont.Controls.Add(this.btnReplace);
            this.frameSoundfont.Location = new System.Drawing.Point(3, 136);
            this.frameSoundfont.Name = "frameSoundfont";
            this.frameSoundfont.Size = new System.Drawing.Size(359, 55);
            this.frameSoundfont.TabIndex = 2;
            this.frameSoundfont.TabStop = false;
            this.frameSoundfont.Text = " Soundfont ";
            // 
            // lblSoundfont
            // 
            this.lblSoundfont.AutoSize = true;
            this.lblSoundfont.Location = new System.Drawing.Point(14, 26);
            this.lblSoundfont.Name = "lblSoundfont";
            this.lblSoundfont.Size = new System.Drawing.Size(16, 13);
            this.lblSoundfont.TabIndex = 1;
            this.lblSoundfont.Text = "   ";
            // 
            // btnReplace
            // 
            this.btnReplace.Location = new System.Drawing.Point(278, 22);
            this.btnReplace.Name = "btnReplace";
            this.btnReplace.Size = new System.Drawing.Size(75, 23);
            this.btnReplace.TabIndex = 0;
            this.btnReplace.Text = "Replace";
            this.btnReplace.UseVisualStyleBackColor = true;
            this.btnReplace.Click += new System.EventHandler(this.btnReplace_Click);
            // 
            // frameLyrics
            // 
            this.frameLyrics.Controls.Add(this.lblLyrics);
            this.frameLyrics.Location = new System.Drawing.Point(3, 75);
            this.frameLyrics.Name = "frameLyrics";
            this.frameLyrics.Size = new System.Drawing.Size(359, 55);
            this.frameLyrics.TabIndex = 3;
            this.frameLyrics.TabStop = false;
            this.frameLyrics.Text = " Lyrics ";
            // 
            // lblLyrics
            // 
            this.lblLyrics.AutoSize = true;
            this.lblLyrics.Location = new System.Drawing.Point(17, 26);
            this.lblLyrics.Name = "lblLyrics";
            this.lblLyrics.Size = new System.Drawing.Size(16, 13);
            this.lblLyrics.TabIndex = 0;
            this.lblLyrics.Text = "   ";
            this.lblLyrics.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sldPosition
            // 
            this.sldPosition.LargeChange = 1;
            this.sldPosition.Location = new System.Drawing.Point(3, 53);
            this.sldPosition.Maximum = 100;
            this.sldPosition.Name = "sldPosition";
            this.sldPosition.Size = new System.Drawing.Size(253, 45);
            this.sldPosition.TabIndex = 4;
            this.sldPosition.TickStyle = System.Windows.Forms.TickStyle.None;
            this.sldPosition.MouseDown += new System.Windows.Forms.MouseEventHandler(this.sldPosition_MouseDown);
            this.sldPosition.MouseUp += new System.Windows.Forms.MouseEventHandler(this.sldPosition_MouseUp);
            // 
            // cmdOpenFP
            // 
            this.cmdOpenFP.Location = new System.Drawing.Point(12, 3);
            this.cmdOpenFP.Name = "cmdOpenFP";
            this.cmdOpenFP.Size = new System.Drawing.Size(401, 23);
            this.cmdOpenFP.TabIndex = 5;
            this.cmdOpenFP.Text = "click here to open a file && play it";
            this.cmdOpenFP.UseVisualStyleBackColor = true;
            this.cmdOpenFP.Click += new System.EventHandler(this.cmdOpenFP_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(12, 29);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(19, 13);
            this.lblTitle.TabIndex = 6;
            this.lblTitle.Text = "    ";
            // 
            // frmMidiTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 193);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.cmdOpenFP);
            this.Controls.Add(this.frameLyrics);
            this.Controls.Add(this.frameSoundfont);
            this.Controls.Add(this.chkFX);
            this.Controls.Add(this.frameTempo);
            this.Controls.Add(this.sldPosition);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMidiTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BASSMIDI test";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMidiTest_FormClosing);
            this.Load += new System.EventHandler(this.frmMidiTest_Load);
            this.frameTempo.ResumeLayout(false);
            this.frameTempo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldTempo)).EndInit();
            this.frameSoundfont.ResumeLayout(false);
            this.frameSoundfont.PerformLayout();
            this.frameLyrics.ResumeLayout(false);
            this.frameLyrics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sldPosition)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox frameTempo;
        private System.Windows.Forms.Label lblBPM;
        private System.Windows.Forms.TrackBar sldTempo;
        private System.Windows.Forms.CheckBox chkFX;
        private System.Windows.Forms.Timer tmrMidiTest;
        private System.Windows.Forms.GroupBox frameSoundfont;
        private System.Windows.Forms.Label lblSoundfont;
        private System.Windows.Forms.Button btnReplace;
        private System.Windows.Forms.GroupBox frameLyrics;
        private System.Windows.Forms.TrackBar sldPosition;
        internal System.Windows.Forms.Label lblLyrics;
        private System.Windows.Forms.Button cmdOpenFP;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.OpenFileDialog CMD;
    }
}

